# Human's role
Although much of the learning process is conducted by computers, there are still key activities that need to be performed by humans:

- Correctly defining the problem is non-trivial in the real world
- Feature engineering 
- Choosing learning algorithm
- Hyper-parameters tuning (but can also ML-tune parameters...)

## Feature Engineering
It is the set of tasks aimed at designing **effective feature sets**.
This is not an algorithmic problem: it usually requires a good understanding of the properties of the given task, practical experience, as well as testing.
Often, the **input** data itself is far less than ideal, either because of noise, scarcity, eterogenity, and therefore it needs to be transoformed in something usable by common ML algorithms.

Feature engineering can be divided in 4 phases:

1. **Feature identification**: deciding which descriptors could be helpful for the task, where to find them, and choosing learning algorithm
   - often non-trivial 
   - databases may not be public
   - acquiring the data may require the use of *web scraping*
2. **Feature extraction**: transformation of raw data into features suitable for modeling
3. **Feature transformation**: normalization, scaling & other modifications to the data aimed at improving the performance of learning algorithms.
4. **Feature selection**: removing unhelpful features.


### Feature extraction

#### Text
Usually a 3-step process is used:

1. Tokenization (splitting text into units)
2. Lemmatization (trace words back to their *lemma*, i.e. base dictionary form, usually meaning **removing inflection**)
3. Encoding, usually into a sparse vector/matrix built against a dictionary

**Embeddings**: alternative approach where words are projected onto *semantic spaces* (with latent meaning learned from context, not explicit).

#### Images

Images can be easily decomposed in pixels, but those lack the meaning of the original object. Therefore, a system over 3 additional layers is often used:

0. Pixels
1. Edges
2. Object parts
3. Objects

Simpler techniques can still be very useful for certain tasks, when some property of the image is relevant (e.g. average brightness, predominant colors...)

#### Geospatial data
Usually required for either **geocoding** (recovering a point of interest from an address) or **reverse geocoding** (recovering the address from the point of interest), for use in turism predictions.

Geospatial data is usually stored as either addresses or coordinates:

- Address processing is often error-prone due to mispelling, requiring error-cleaning
- Coordinates contain fewer errors, but are often imprecise
    - GPS noise or weak signal in certain places
    - unreliable localization through WiFi networks (often used by mobile devices)

In urban areas, useful features derived from geospatial data may be distances from critical infrastructures or attributes of the building; outside urban environments, natural features such as height above sea level, meteorological data etc may be useful.

#### Date and time

This type of data is often used to detect frauds.

Days of the week can be easily encoded in a binary matrix, possibly with additional variable `is_weekend`.
Some tasks may require more specific features (e.g. a `payday` variable for a given day-number could be invaluable in monitoring cash withdrawals).

Time is harder to deal with due being a real value with non-natural order around day changes ^[e.g. `0:00` < `23:59` in real variables, but `0:00` might also be `24:00` of the current day]. To deal with this, a common approach is to use a **circle projection** of the recorded date time, as this preserves the **relative distance** between the data points (which is a necessary step for using several ML algorithms).

#### Time series

Data composed of an ordered series of time-stamped events. Often used to understand useful or dangerous **peaks** in the subject matter.

#### Other domains

Dealing with other types of data may require basing features on own's intuition regarding the nature of the data, depending on available information and nature of the classification task.

## Data processing *(feature extraction)*

### Categorical Encoding
Certain algorithms require specific **format** of data; particularly, most algorithms (with the notable exception of decision trees) disallow **categorical** data.

To get around this problem, categories must be **encoded** in a numeric format. 
The simplest approach (**label encoding**) is to assign an integer to represent each category, however this will create problems for algorythms in which the weight of each attribute matters, creating a bias towards higher labeled categories.
Additionally, even among the other algorithms, label encoding of non-ordinal data may introduce an **artificial order** on the categories, which may affect precision (e.g. binary decision trees as implemented in `scikit-learn` ^[see <https://stackoverflow.com/a/56857255>])

The standard approach is therefore to use **one-hot encoding**, which encodes variables in a vector with all zeroes except a single 1 corresponding to the category (**one hot vector**). 
This means transforming a feature value into an array of dimension $N\equiv$ the number of possible categorical values, therefore it may not be a good idea for large $N$ as it will introduce a very large number of *dummy features*.

An alternative to those cases may be to utilize **label binarization**, which introduces only $log_2(N)$ values instead of $N$.

### Normalization techniques
Many algorithms suffer from **unbalanced scaling** of feature data, i.e. they attribute more weight to features that register larger *absolute differences* in values. This is usually undesirable, as generally the amplitude of the deltas are due to differences in measurement metrics.

To solve these problems, it is often necessary to process the data with **normalization** techniques.

#### Centering
Done by subtracting the sample mean from all values: 
  
$$\overline{X} = \frac{\sum^N_{i=1} X_i}{N}$$

#### Scaling
Done by dividing all values by its sample standard deviation:

$$SSD_X = \sqrt{\frac{1}{N-1} \sum_{i=1}^N (x_i - \overline{x})^2}$$

#### Log scaling

When the data is not *normally distributed*, an **asymmetry** can be observed: the curve will be skewed either to the left or to the right of the most frequemt value.
**Skewness** can be used as a method of evaluating how far the curve is from a normal (i.e. **Gaussian**) distribution, which negatively affects the performance of several algorithms.

A common way to reduce the skewness of feature values is to perform **log scaling**, i.e. apply a change of base.
Very sparse distributions may require more complex methods (e.g. `qqnorm`) ^[see `Scikit-learn` documentation].

### Missing values

Real world datasets often have some missing values, due to data corruption, recording failures etc, which must be dealt with somehow when processing a dataset.
The simplest way is to simply **drop** instances with missing values, if they are few (e.g. < 10%), or even drop the feature itself if it has > 50% missing values.

Another possibility is to **replace** the value with either a *neutral* or "best guess" value depending on the rest of the data:

- for **numerical** values, the standard approach is to use the mean, median, or mode of the entire distribution
- for **categorical** values, use the most probable value, but it could be dangerous ^[why?]

#### Regression imputation

A more complex approach to missing values is to estimate a **regression model** in order to predict the values of feature $x_j$ based on the values of the other features $x_k...x_n$: 
$$
x_j = w_k x_k + ... + w_nx_n
$$  

Knowing the **correlation matrix** of the features can be particularly helpful in designing the model, as a set of correlated features may jointly be a good estimator for the missing feature.

#### $k$-nearest neighbours

Another approach is to consider the $k$ complete instances that are *most similar* to the instance missing the value for feature $x_j$, and impute the value from the most frequent value for $x_j$ over those instances (**mode**).

### Data augmentation

In certain cases, it may be helpful to *add* **derived features** to the dataset, depending on the format (e.g. variations by rotation, scale, interpolation etc. for images, or elapsed time between two relevant features in timeseries format).

It can also be beneficial to add *artifical instances* in cases where the **classes** are too **imbalanced** for the task of identifying a *minority class*.

A common case where datasets will be unbalanced is **anomaly detection**, i.e. a task that consists in finding the **outliers**, e.g. for fraud detection.


#### Sampling

A simple way to balance datasets is to either **oversample** instances of the minority class (i.e. add copies), or **undersample** instances of the majority class (i.e. drop some).

There are also more advanced sampling techniques such as **Synthetic Minority Oversampling Technique** *(SMOTE)*, which creates new instances of the minority class by convex combinations of neighboring instances.

#### Cost-sensitive learning

In usual ML all missclassifications are treated equally, however there are tasks where it is in fact desirable to obtain an *imbalanced classification* which will discriminate between them. 

**Cost-sensitive learning** uses a **cost function** $C(p,t)$, usually in the form of a matrix, which specifies the cost of missclassifying an instance of $t$ as $p$. The goal thus becomes **minimizing predicted cost**, rather than straight model accuracy.


## Feature selection

Number and complexity of required features depend on the specific task being addressed. While using as many features as possible may seem logic, there are disadvantages:

- **potentially correlated features** may not add useful information, and potentially even *decrease* model performance
- as the number of feature grows, the model becomes **less interpretable** and **less generalizable**

Exhaustive search for optimal feature subset is usually infeasible, therefore several search strategies have been proposed.

### Filter methods
**Filter** methods use a classification algorithm *independent* of the chosen learning algorithm, and can therefore be used before selecting it.

They use various metrics of distance, similarity, correlation between features, of consistency, of information content.

They may evaluate single features (**univariate**) or an entire subset (**multivariate**).

Notable:

- Information Gain (IG)
- **Relief**: estimate feature quality by its ability to distinguish instances that are similar, by comparing the *nearest hit* and the *nearest miss* of each selected instance. 

### Wrapper methods
**Wrapper** methods estimate feature quality by their performance in a specific learning algorithm, by directly testing various subsets, usually decided by some heuristic search ^[exhaustive search in feature set $V$ would require testing $2^{|V|}$ subsets, which is not tractable for large $V$]. 
They are generally much slower than filter methods, and can be divided in:

- Sequential feature selection algorithms
- Recursive feature elimination (inverse of above)
- Genetic algorithms

### Embedded and hybrid methods
**Embedded** methods perform feature selection ***during*** the execution of the learning algorithm, i.e. they cannot be separated form the learning phase at all, and are usually model-dependent.

The most common are:

- **Perturbation**-based approaches
- **Gradient** approaches