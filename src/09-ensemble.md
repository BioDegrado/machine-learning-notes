# Ensemble methods
A strategy that consists in combining several predictors through some generalization strategy, in order to reduce the risk of selecting a classifier that is particularly unsuited to the task. In particular, it can reduce *variance* and *bias* components of the error.
To reduce overfitting, ensemble methods can be used to train different classifiers on different partitons of the data.

$$Var(Ensemble(..)) = ?$$

The probability that the ensemble classifier missclassifies an istance is the probability that more than half of the classifiers are wrong, as long as the classifiers $h_j(x)$ are independent, which is usually very low as long as each classfier is right ~50% of the time.

Each classifier may use different data, or even different learning algorithms.

## Constructing ensembles

- Distribute the training set between predictors
- Distribute input features
- Manipulate class labels (?)
- Manipulate learning algorithms

### Base classifiers

- Linear classifiers
- DTrees

## Homogeneous ensembles
Use a single learning algorithm over distinct data sets (*may* contain some overlap).

- Bagging:
- Boosting:
- Random forests: manipulate features
- Decorate: add artificial training data 

### Bagging
Bootstrapping + aggregating. Good when data has high variance.
It consists of repeated **extractions with replacement** from the training data, often with same cardinality as the original set *(0.632 bootstrap)*.

Istances that were not extracted are used for testing.

Each istance has $\frac{1}{n}^m$ of being selected for training and $(1-\frac{1}{n})^m$ for testing.

If $n = m$, the probability of being selected for testing is 0.368

#### Combining: voting
By combining several linear separators, we may obtain perfect or near perfect classifiers for many istances, including those that would otherwise require non-linear models.

Two methods of voting:

  - **Majority** voting
  - **Weighted** voting, i.e. the classification function is some linear combination of the predictors (can be machine learned by perceptron too).

#### Pros
- Surprisingly competitive performance
- Rarely overfits
- Good ability to ignore irrelevant features

#### Cons

- Ill-suited to classifying datasets where istances are of different complexity, since it assumes equal relevance of all data samples.


### Boosting
Add models to the ensemble **sequentially**: at each step, a new model is created and trained (i.e. updated) from the errors of the previous model, by increading weight (i.e. probability of extraction) of missclassified samples.

The main goal of this method is **reducing bias**.

#### Combining: *AdaBoost*
Adaptive boost *(AdaBoost)* 

[?]

- Testing on individual classifiers at the end of each round
- ?


Classifiers: $C_1, C_2...C_n$

Loss function (error rate of $C_i$ on sample $D_i$): [?]

Weight updating:
$$w_j^{i+1} = w_j^i * \begin{cases}
   exp^{-\alpha} &\text{if } C_i(x_j)=y_j \\ 
   exp^\alpha &\text{otherwise}
\end{cases}$$


[Pseudocode ?]


### Random forest
A popular ensemble of **unpruned decision trees**, and one of the highest-performers among non-deep learning methods.

Tree generation:

- bagging method: each tree trained using a bootstrap sample of training data
- random vector method: at each decision node, the best atribute to test is chosen from a random sample of $m$ attributes, rather than all, according to some criteria like $InfoGain$.

Randomization helps to reduce unwanted correlation among features as well as bias in the model.

### Decorate
Samples are hand-picked according to be interesting in some way, and used to create a first base learner to tag the data. Then, [?].

# Support vector machine (SVM)
A classifier derived from statistical learning theory (Vapnik, 1992).
It is an algebraic method to learn an arbitrary discriminating function $f$ which can be either linear or non-linear.


## Linear SVM
The goal is to learn $f(x) = W^T x+b$,  where $W^T$ is the transposed matrix/vector form of [?].
$f(x)$ is a hyper-plane in the n-dimensional feature space.
Using $+1/-1$ binary values, the classifier for the given problem is then the sign of $f$.

### Margin
Note that each problem has infinite possible separators therefore infinite solutions. The best classifier is then the one with the **largest margin**, i.e. the one that has the largest "safe area" between instances of different classes. This is because the area closest to the separator has the highest uncertainty, i.e. instances close to the separator are less reliably classified.

Geometric margin:
$$gm(x_i)=?$$

#### Support vectors
**Support vectors** for the margin are lines obtained by finding the closest points from the two different classes.
Since they are scale-invariant, we apply a scale transformation to $w$ and $b$ to obtain:
$$
H_1: w^T x^+ +b= +1 \\
H_2: w^T x^- +b= -1
$$

The objective is therefore to find $w$ and $b$ that maximize $\frac{2}{||w||}$, which is equal to minimizing $\frac{1}{2}||w||^2$, while maintaining that $y_i(w^T x_i  +b) \ge 1$. 
This is a **quadratic problem** with linear constraints, which can be rewritten by *Lagrangian method* as
$$
\text{minimize } L_p(w,b,\alpha_i)=\frac{1}{2}||w||^2 - \sum_{i=1}^n ...
$$

----

#### Lagrangian functions
Given a function f(x) and a set of constraints $c_1...c_n$
[?]

----

#### Hard margin

Even if the dataset is very sparse, the solution depends solely on the support vectors, since they are the only points with $\alpha >0$.

[?]

----

#### Kernel functions
Functions such as the inner (dot) product are **similarity** functions between vectors, known as **kernel functions**.

[?]

All kernel functions can be written as the inner product between two vectors, projected by a mapping function $\phi$ into a higher plane.

$$
K(x_1,x_2)=\phi(x_i)^T\phi(x_j)
$$

Common kernel functions:
- linear
- polinomial
- gaussian
- ?



----

#### Soft margins
All of this only works for non-noisy datasets. To generalize it, we might try to use non-linear separators, but there is high risk of overfitting. We can then decide to introduce some error tolerance by using **slack variables**, which define max tolerable error in order to obtain a hyperplane that accepts *some* missclassifications, i.e. that classifies *quasi-linearly* separable data.


#### Kernel trick
If the data is still too hard to classify with quasi-linear separators, then it might be possible to map it to a higher dimensions space before trying to find the hyperplane, by using a kernel function.

$$
f(x) = ..\phi
$$

Which kernel function is bets suited for the data is another hyper-parameter.

## Algorithm: SVM

`SVM(?)`

1. Choose kernel function
2. Choose `C`
3. ?


## Characteristics of SVM learning

### Advantages

### Issues

- Sensible to feature scaling, therefore it is essential to **normalize** input data.
- Also sensible to unbalanced data