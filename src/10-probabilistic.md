# Probabilistic learning

Two formulations:

.|name|desc
-|-|-
MLE | Maximum likelyhood estimation|
MAP | Maximum aposteriori probability|

## MLE
Model the joint probability $P(D;M_\theta)$ where  $M$ is a probabilistic formulation (model), $\theta$ are the model parameters and $D$ is the dataset.

### Likelihood function
Expresses the joint probability of a sample data given a set of model parameters:

$$ $$

$f$ is a function of the parameters
case|params|f
-|-|-
discrete case| params are probabilities $P(X_i=x_i)$|$L_d=?$
continuos case| params are coefficients in a probabilistic formulation ...|?

#### MLE Principle
For sufficient statistics, choose parameters that **maximise** ?.

This can often be simplified by maximizing the **log-likelihood** function:
$$ l_D(\theta)=?$$

[?]

#### Binomial distribution model
?
$$l_D(\theta)=N_1\log\theta+N_0\log\theta $$
where ?

#### Multinomial distribution model
?

PMF:
$$P(D;\theta) = ... =  \prod_{i=1}^n \theta_j^{r_j}$$

Joint distribution:
$$L_D(\theta)=P(D,\theta)=\prod_{i=1}^n P(x_i;\theta) = \prod_{i=1}^n \theta_j^{r_j} \text{ where } ?$$


## MAP
Main difference is that MAP is a **Bayesian approach**, while MLE is a **frequentist model**.


$$y^{*} = argmax_{y_j}(P(y_j|x)) = argmax_{y_i}\left(\frac{P(Y=y_i)P(X=x|Y=y_i)}{P(X=x)}\right) \approx$$
$$ \approx argmax_{y_i}\left(P(Y=y_i)P(X=x|Y=y_i)\right)$$

## Naive Bayes classifier
A classifier that utilizes both MLE and MAP.

### Naive Bayes assumption
All the random variables that represent the feature set are **statistically independent**.

[?]
