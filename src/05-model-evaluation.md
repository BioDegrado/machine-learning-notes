# Evaluating a model

Evaluating the performance of a model is a key aspect of machine learning, as we are interested in the performance of the model on new (i.e. *unseen*), **independent** data. Additionally, a good evaluation may allow us to compare *alternative models* (hypotheses) and figure out the most suited for the current task.

## Performance metrics

The performance of a model is usually reported in the form of a **confusion matrix** (also *contingency table*), which (for ***binary*** classifications) will show:

Correct prediction | Incorrect prediction
-|-
TP | FP
TN | FN

The four numbers can be combined in several ways to obtain different metrics:

- $\Acc = \frac{TP+TN}{TP+TN+FP+FN}$ : It measures the general likelyhood of the model to get a correct prediction.
- $\Err = 1-\Acc = \frac{FP+FN}{TP+TN+FP+FN}$ : error measure
- $\Prec = \frac{TP}{TP+FP}$ : the precision tells of all the positives that he recognized the percentage of correct ones.
- $\Rec = \frac{TP}{TP+FN}$ *(True Positive Rate/Recall)* : the recall measures the ability of the model to recognize the positive class.
- $\Fsc = 2\frac{\Prec \cdot \Rec}{\Prec+\Rec}$
- $\Fpr = \frac{FP}{FP+FN}$ *(False Positive Rate)* 

### Common curves

By plotting $\Rec$ against $\Fpr$ we obtain the *Receiver Operating Characteristic* (**ROC**) **curve**, which illustrates the performance of a binary classifier at varying learner settings (parameters, dimension of learning set etc.). The area under the curve (**AUROC** or **AUC**) is a measure of *separability*, i.e. how much of the complete sample space the model is capable of distinguishing.

> If we want to compare two models using a ROC curve, then the best one is that whih is closer to the point [0,1], that is $TP=1$ and $FP=0$. 

The *Precision-Recall* (**PR**) **curve** has a similar scope.

Finally, the **learning curve** plots model accuracy against size of training set, and can help when determining whether the model can be still be improved with more data, or if it has converged to the best possible performance. Additionally, it can be used with different learners to determine which model is best suited according to the amount of training data available for the given problem. 

## Error

> Total Error = Bias$^2$ + Variance + Irreducible Error

- **Bias** is an error introduced by erroneous assumptions made by the learning algorithm.
- **Variance** is an error introduced by the algorithms's sensitivity to *fluctuations* in the training set.
- **Irreducible error** is the remaining part, which cannot be eliminated no matter how good the model is. ^[due to what?]

### Bias

There are several types of bias:

Bias                | Description
-|-
**Sample** bias     | Collected data is not actually representative of the whole population.
**Exclusion** bias  | Some key features are missing in the data.
**Observer** bias   | Tendency to design experiments that prove designers' pre-made hypothesis.
**Prejudice** bias  | Selected data is representative of a prejudice over the population, and will pass such prejudice to the model.
**Measurement** bias| Data is actually altered by/during the observation.

### Variance

It is defined as the mean of the squared differences between $N$ individual outcomes $x_i$ and the mean $\overline x$ ^[also written as $E(x)$ in literature].

$$ \Var (x) = \sigma^2 = \frac{\sum_i^\infty (x_i - \overline x)^2}{N} $$

## $k$-fold cross-validation

Cross validation is a technique commonly used to **minimize the variance** in assessments of a model's precision, whenever enough training data is available.
It works by conducting multiple $(k)$ indipendent random trials (on different splits of training and test data) and averaging the results, in order to reduce the probability of selecting *lucky* or *unlucky* (training set, testing set) pairs.

### Estimator

It is a function used to estimate qualities of the data from which the sample is drawn.

It is formally defined as: 
$$\hat\theta_S = g(S)$$
for $S = (x(1),...,x(m))$ and $x(i) \sim D$ ^[$x(i)$ random var. drawn from distribution $D$]

> A simple estimator is the **mean**.

Given estimator $x$ and the value $y$ it is predicting:

- **bias** measures how close the estimation is to the real value: $\Bias(x) = E[x] - y$. An *unbiased* estimator has zero bias.
- **variance** measures how much the precision varies between different samples: $\Var(x) = E\left[ (x - E(x))^2 \right]$


## Hypothesis testing

Given a specific ML algorithm $L$, and a model $h$ learned by $L$ by training on dataset $S$ of $n$ samples ^[on a fixed set of hyper-prameters], we want to estimate its **prediction accuracy**.

This means finding the best estimate of the accuracy of $h$ over *unseen* samples with same distribution as $S$.

To do this, since we do not know the **true error**, we must approximate it using the **sample error**.

We can say that the number of errors $r$ commited by our hypothesis on a set of dimension $n$ follows the binomial distribution:

$$
	P(X = r) = {{n}\choose{r}} p^r(1 - p)^n-r
$$

$p$ reppresents the probability of our model to commit a single error. This value is the same for the true error, and for the sample error, assuming that all the elements of the subset $S$ are picked indipendently and randomly.

**In other words this distribution can describe the behaviour of either the sample error or the true error.**

\begin{align*}
	E[X] = np \\
	Var(X) = np(1-p) \\
	\sigma_X = \sqrt{np(1-p)}
\end{align*}

### Sample error *(error rate)*

It is the fraction of istances $h$ missclassifies on the subset $S$ of all the instances in $D$:

$$
	\Err_S(h) = \frac{1}{n} \sum_{x\in S}\sigma(\ f(x),h(x) \ ) = \frac{r}{n} = 1 - \Acc_S(h)
$$

Where

\begin{align*}
	\sigma(x, y) = 
	\begin{cases}
		1 & \text{if  } x \ne y\\
		0 & \text{if  } x = y
	\end{cases}
\end{align*}

thus $\sigma$ in our case just counts the number of errors committed.

### True error

It is the probability that $h$ will missclassify *any* instance $x$ drawn at random according to distribution $D$, compared to the unknown *target function* $f$.

$$error_D(h)=P_{x\in D}\left[f(x)\ne{h(x)}\right]$$

### Sample error as estimator of the true error

The problem with the true error is that to calculate it we need to sample the entire set of all possible instances, and then compute the error normally. This is clearly improbable for most problems, since we are stuck with a small subset $S$ of instances of the entire set.

So the question is : **can we use the sample error as if it's the true error?**

Or in other words does $\frac{r}{n}$, in average, converges to $p$?

$$
	\frac{r}{n} - p = 0
$$

Since we know that $r$ is binomially distributed, then in average it tends towards $np$, thus:

$$
	\frac{np}{n} - p = 0
$$

Hence even though the true error is usually unknown, $\ErrS$ can be used as an **unbiased estimator** for it by conducting $k$ trials and averaging it.

Another useful measurement is the standard deviation $\sigma$ of $\ErrS$. We know that $r$ follows a binomial distribution, while $n$ is constant, so the variance depends only on $r$.

\begin{align*}
	\sigma_S &= \frac{\sigma_r}{n} \\
	&= \sqrt{\frac{np(1-p)}{n^2}} \\
	&= \sqrt{\frac{p(1-p)}{n}} \\
	&= \sqrt{\frac{\ErrS(1-\ErrS)}{n}} 
\end{align*}

By the **Central Limit Theorem**, the normalized sum of a large number of independent random variables tends towards a **normal distribution**, even if the original variables are not normally distributed, and the mean of said sum will tend to the mean of the real population measured by those $r.v$.

We can use this to conclude that, even though $\ErrS(h) \not ={\ErrD(h)}$, the normalized error rate sum over $k$ trials $\ErrS_k(h)$ is a binomial $r.v.$ that can be approximated by a normal distribution, therefore its mean $p$ tends to  $\ErrD(h)$.

In other words:

$$E_{(k)} \left[\ErrS \right] \to  E_{(k)}[\ErrD] \quad \text{for large } k$$

In practice, usually $k \ge 30$ is considered sufficient ^[why? Something to do with the shape of the gaussian with such parameter].

----

#### Gaussian distibution

A common distribution represented by a curve perfectly **symmetric over the mean**.
It can be identified simply by its mean and standard deviation ($\mu, \sigma$).

Notably, the curve has:

- $68.3\%$ of **probability mass** within $\sigma$ from $\mu$, i.e. $f(x) < \mu \pm \sigma$.
- $95.4\%$ within $2\sigma$.
- $99.7\%$ within $3\sigma$.

The algebraic sum of normal distributions is *still* a normal distribution, with variance equal to the sum of the variances.

----

#### Confidence interval (CI)

It is a lower and upper bound for the estimated error rate.

$$\Delta = | error_D(h) - error_S(h)| \le ME \rightarrow |p- r/n| \le ME [TODO]$$

> **Def:** An $N\%$ **confidence interval** for some parameter $p$ is an interval that is expected with probability $N\%$ to contain $p$.

Since we can use a normal distribution to estimate the confidence interval we can just do a look up table:

$$
	\text{CI} = \left[ \Err_S(h) - z_N \sqrt{\frac{\Err_S(h)(1 - \Err_S(h))}{n}}, \Err_S(h) + z_N \sqrt{\frac{\Err_S(h)(1 - \Err_S(h))}{n}} \right]
$$

Where $z_N$ can be looked on the normal distribution table, also we used the variance of the binomial distribution as the variance of the normal one, this is yet another approximation.

#### One side bound

Probability that the error is *at most* a given value.

## Comparing hypothesis

When evaluating hypothesis, the observed accuracy ordering may *not* reflect the real accuracy order, depending on the error rate precision.
To understand whether we should rely on the observed accuracy, we should use a *null hypothesis* that states the two hypothesis are in fact the same, and attempt to prove it by one the methods described below.

### Two Tailed Test 

Given a ML algorithm, let

- $h_1\equiv$ two hypothesis are different, 
- $h_0\equiv$ they are the same.

*Common wisdom* is to reject the null hypothesis if $p<0.0013 \ (0.13\%)$ ^[why?]

$(|d\ne0|)$ [?]

### One-right tailed test

**Exercise**:
r =10 errors
n = 65
90% confidence interval (two-sided) for true error rate?
95% one-sided interval?
90% ''' ?

**Solution**
$$\ErrS = 10/65 = 0.154
SD = \sqrt{r/n(1-r/n)n} = 0.045
N = 90\% \implies z=?$$
> [? unfinished]

[*]: from *z-table*
