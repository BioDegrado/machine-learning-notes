% Machine Learning notes
% Andrea Castellani
% fall 2019

# Introduction to  Machine Learning

## Machine learning
Machine Learning is a **set of metodologies** to find regularities in data, and use those to **predict** future outcomes and/or classify potential new data. It is meant to obtain systems that can automatically improve with experience, i.e. **learn**.  
It has related disciplines, as well as dedicated applications, in nearly every field.

**Example**     A model to determine whether a home is in San Francisco or New York.  

>  Start with data that is already **classified**: from this you can learn that *building elevation* can be a good criteria (above a certain threshold, all homes must be in the hilly SF).
>  But under the threshold, more info on the subjects, called **features**, are needed, such as price: prices are higher in NY, therefore high prices probably suggest NY, when in doubt.
>  In reality, proper partitioning could require year built, #bathrooms, #bedrooms, aparment dimensions etc.

### Feature
Attribute/descriptor of the subjects of the collected data.

### Datamining
Process of finding *patterns* that can lead to **boundaries**, i.e. rules that allow the algorithm to decide to which class each item should belong to.

### Data partitioning
Data is properly **partitioned** when each partition holds only data subjects of a certain class/type.


## Learning
To make sense of a concept, event or feeling.

There are 2 main methods humans use for learning:

- Trial and error
- By example, or otherwise externally acquired knowledge (e.g. books)

Not everything is *learnable*, not by humans and not by machines. There are also things that are possible yet difficult to learn or to teach, and they are often not the same for humans and machines, which is why machine learning is so interesting.

### Hard to learn

- Humans can easily make sense and draw conclusions from small samples of data, but they have great difficulty analyzing large samples.
- Knowledge that is *not* desirable to learn by trial and error, because errors would cause grave or even unacceptable consequences.

### Hard to teach
Concepts that require a large amount of *previous expertise* not directly conveyed in the data, possibly because we ourselves do not currently understand how our brain processes it.

> **Examples**: speech and image recognition.

### Most suitable cases for ML

- Humans lack expertise to complete the intended task.
- Humans would be put in danger to learn such expertise.
- It would be clearly too expensive (time, money) to employ humans for the task.
- Data is very large and/or changes frequently.
