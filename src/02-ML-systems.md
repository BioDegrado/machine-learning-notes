# Building a ML system

## Goal
**Learning** is an **optimization** problem, either in *minimizing error* (deviation from expected/desired results) or *maximing reward* (based on policy)

## Hyper parameters
Parameters whose value must be set before initiating the learning process (e.g. maximum depth of a decision tree).

## Model
**Input data** & **Output** (results of given data) --> **Program** that attempts to predict output for future input data.

## Data
Data may need:

- Cleaning
    - errors
    - redundancies
- Preprocessing  
    - Renaming
    - Normaliation
    - Discretization
    - Abstraction
    - Aggregation
    - etc

## Feature selection
The size of a given sample can be untractable, therefore it is usually necessary to restrict the processed data to a **subset of features** of the subjects. 

- **Dimensionality reduction** : replace input with a more high-level description (e.g. *words of happiness*)

## Model selection
Involves deciding which model category to use among:

- **Supervised** inductive learning: training input is associated with desired *output labels* (values of the output function). Model must learn to **classify data**.
- **Unsupervised** learning: training data does *not* include output labels. Model must *find* similities in order to **create a classification**.
- **Semi-supervised** learning: training data only includes output labels for a subset of the input.
- **Reinforcement** learning: the aim is not to learn a classification, but to learn a **policy** (common in *robotics*). This is accomplished by an **evaluation function** (also called **fitness** function) which *rewards* a desired behaviour or accomplishment, so that the machine will try to develop a **strategy** to maximise the reward.

### Supervised learning
Given ($x$, $y=f(x)$), learn $f(x)$ to predict its values for unseen $x$:

- $f(x)$ is *discrete*: **classification** problem
- $f(x)$ is *continous* (algebraic): **regression** problem, e.g. *Neural Networks*
- $f(x)$ is a *probability*: **probability** estimation

#### Classification
Given a collection D of istances (training set), feature vector $v(x)= <v_1, v_2, \dots, v_n>$, find a model for predicting the label of the **class** feature.
Istances can be represented as *records* in a table, or *vectors* in a multi-dimensional space.

Domain and co-domain can be both discrete or real-valued.

### Reinforcement training
Reinforcement learning is about an having an *agent* (the model) learn to complete a task in an enviroment described as a set of states where it can do a limited number of actions. If the action helps it to reach the completion of the task it is **rewarded**, if it hinders the task it's punished, and every other action can be ignored.

In practice the steps of the algorithm (**training** phase) can be summarized as:

1. Observe state.
1. Select an action.
1. Observe new state.
1. Evaluate reward.
1. Repeat. 

## Testing Phase
The model is subsequently tested by comparing the output of the model on new (*unseen*) input data to its real or desired result, and measure the error. The error is the main way to determine how **precise** (and thus useful) the model is.

In any case, the result is an **estimate**, since it is calculated over a subset of the data. It is thus recommended to test the **statistical significance** of the results.

## Interpretability
Ability of the model to **explain** the prediction it makes. In general, the more complex (e.g. number of features, number of decisions) the model is, the harder it is to have good interpretability.
Many models are not interpretable *at all* (e.g. neural networks).
