# Appendix {.unnumbered}

**Project info**

- Any non-trivial, real-life or invented problem (student's choice).
- Use a large dataset, possibly more than 1.
- There must be some pre-processing of data
- Test more than 1 algorithm, perform hyper paramter tuning
- Final analysis of results that demonstrate understanding of result is *more important* than precision of the created model.
- Original algorithm design is *not* required, but accepted.
- Should be doable in ~2 weeks.
- Worth 25% of final grade.