# Unsupervised learning (clustering)

Also known as **segentation** or **classification**, it's a method that aims to group istances into unknown classes (**clusters**) according to a measure of *similarity* (**distance**).

Clustering is usually easy in bidimensional spaces, but can be a very complex problem for multidimensional datasets.

## Distance
The actual definition of distance depends on the type of data being analyzed, but in all cases some properties must hold:

- Symmetry
- Constancy of self-symmetry
- Positivity (separation)
- Triangular inequality

Measurement unit may affect the cluster, therefore data should be standardized.

### Mean absolute deviation
?

### Minkowski distances

?

famous distances|q
---|-
Manhattan distance|1
Euclidean distance |2

### Contingency tables
?

#### Simple matching coefficient (SMC)
In this measure, the mapping of binary values does not impact the result of the computation.

$$d(x_i,x_j)=\frac{b_{01}+c_{10}}{...}$$

#### Jaccard coefficient
In this measure, binary mapping precisely means the absence (0) or presence (1) of a certain property, which is a notion utilized by the computation of distance.

$$d(x_i,x_j)= $$

### Nominal variables
If the variables are non-binary (nominal or categorial), different methods must be employed.

### Ordinal variables
Variables, discrete or continuous, where **order** is important can be  **interval-scaled**:

- processed into a **rank** relative to the dataset
- mapped into ?

### Ratio-scaled variables
?

### Mixed variables
?

#### Complex data types
Example: string distances

**Edit distance**

## Major metodologies

methodology|description|examples
-|-|-
Partitioning|construct various partitions and then evaluate them by some criteria|?
Hierarchical|create a hierarchical decoposition of data|?
Density-based|?|?
Model-based| ?|Gaussian Mixture Model (GMM), COBWEB
Spectral clustering|?|?
Clustering ensembles|Combine multiple clustering results (i.e. different partitions)|?

### Hierarchical clustering

#### Agglomerative (bottom-up)
`HAC`

1. Begin with a matrix of distances between pairs.
2. Each item is initially in its own cluster. 
3. At each iteration, merge *closest*[^1] clusters.
4. Repeat until all clusters are merged together.

The algorithm outputs a *dendogram*, which can be cut at any desired level to obtain clusters with the desired generality.

Outliers are easy to notice becase they are the last items to be aggregated, and thus end up clearly separated form the rest.

[1]^ Cluster similarity can be computed by several methods:

name|desc|math
-|-|-
single link| uses minimum distance of pairs| $sim(c_i,c_j)=\max_{x\in C_i,y \in C_j} sim(x,y)$
complete link| uses maximum distance of pairs| $sim(c_i,c_j)=\min_{x\in C_i,y \in C_j} sim(x,y)$
centroid-based| uses average of vectors in each cluster (i.e. distance between  virtual points which have values equal to mean values of their cluster) | ?

`K-means method`
?

#### Divisive (top-down)