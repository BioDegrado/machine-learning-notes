# Decision trees
Data structure in the shape of a tree, where nodes are **tests** on feature values, with a branch for each possible value, and leaf nodes are **decisions** (i.e. they specify the **class label**).
It can be rewritten as a set of rules in **DNF** (*Disjunctive Normal Form*).
This algorithm requires discrete data and involves **greedy** recursive partitioning, which can produce errors: often there is no model that can classify accounting for *all* features in exam with 100% precision.


## Decision tree induction

Repeteadly partition the data until classification is complete. **Greedy** as it makes only *local* choices.

Goal is to have the smallest decision tree possible, though finding the **minimal decision tree** is a *NP-Hard* optimization problem. This algorithm is a heuristic for it.

**Note:** It is possible that some descriptors are never utilized.

## Algorithm
 
`DTree` (*examples*, *features*) : tree

- **IF** all remaining examples have the same classification, return it
- **ELSE IF** remaining feature set is empty, return most frequent classification
- **ELSE**:
   1. Pick feature $f$ with the **highest IG** and create a node $R$ that tests on it
   2. **FOR** each possible value $v_i$ of $f$, let $s_i$ be the subset of examples that take value $v_i$ on $f$:
      1. Create a an edge $E$ labeled $v_i$ connected to $R$
      2. 
         - **IF** $s_i$ is empty, create a node connected to $E$, labeled with the most common classification in examples
         - **ELSE** call `DTree`($s_i$, features - {$f$}) and add the resulting subtree to $E$
    3. Return (sub)tree rooted at $R$

## Entropy
Estimate of the **uncertainty** of a given classification, ie a measure of *disorder*.

$$ E(D)_{binary} = p_1 *(-log_2(p_1)) + p_0*(-log_2(p_0)) = - p_1 *log_2(p_1) - (1 - p_1)*log_2(1 - p_1) $$  

where $p_0, p_1$ are respectively the fraction of examples belonging to class $0$ or $1$.

Therefore if all examples are in one category, entropy is the minimum $-1*log(1) - 0*log(0) = 0$ ^[we consider $0*log(0)=0$]; if they are equally mixed between the two categories, it is the maximum $-0.5*log(0.5) *2 = (-0.5)*(-1) *2 = 1$.

Entropy can also be viewed as the number of bits required to encode the class of an example in D.

### Multi-class entropy
The above formula can be generalized to any number of categories.
Given a classification in $n$ classes and the associated set of probabilities $p_i$  $(0\le i \le n-1)$, entropy is defined as
$$ E(D)_{multi} = \sum_{i=0}^{n-1} -p_i*log_2(p_i)$$

## Information gain (IG)
IG of a feature *f* is the **expected reduction in entropy** resulting from utilizing the feature to split the data.

This equals to the current entropy minus the sum over the entropy of each newly created subset, weighted by relative subset size.

$$IG(D,f) = Entropy(D) - \sum_{v\in Values(f)}\frac{|D_v|}{|D|}Entropy(D_v)$$

where $D_v$ is the subset of $D$ that take value $v$ for feature $f$.


## Gini
There are several alternative criteria to $IG$ [see book].

## Measures of confidence for rules
### Support
Each rule has **support** $\frac{|D_v|}{|D|}$ = relative size of data subset that satisfies the rule. This is important to compute the statistical relevance of the rule.

### Confidence
Rules can also be evaluated by **confidence**, i.e. subset of $D_v$ which is correctly classified by a rule. This can be viewed as the (estimated) probability that the rule outputs a correct decision.

## Characteristics of Decision Tree Learning

- Continuos features must be handled:
    - either  by *discretizing* the feature's values
    - or by using **regression trees**
- Fast because greedy, only a single scan of the data
- Can handle fairly noisy training data
- There are methods to handle **missing** feature values
- **Overfitting**
  
### Complexity

- **Worst case**: complete tree where every path tests every feature. Assuming $m$ features and $n$ examples, complexity is $\sum_{i=1}^m i*n = O(nm^2)$ 
- In practice the resulting tree has $|leaves|\ll n$ therefore $O(n*m)$.

### Overfitting
Model may adapt *too* much to the input data, losing in generalization power. This means that the model is not only learning patterns, but also exceptions, i.e. potentially adapting to noise.

A hypothesys $h$ is said to **overfit** the training data if $\exists h'(\ne h)$ that performs worse than $h$ on training data, but better than $h$ on unseen data.

Common causes:

- **Noise** in category or feature labels, as any error in training data will result in special cases if the algorithm is not properly configured.
- Making decisions on **unreliable rules** (i.e. that have low confidence/support). This can easily happen **towards the leaves**, as the remaining examples may be very few.
- Missing some/all key features for the problem being analysed (**bad feature engineering**)

There are two main prevention techniques:
- Pruning
- Reduced Error Pruning

#### Pruning
Stop growing the tree whenever $|D_v|<k$, i.e. there is no sufficient data (according to a hyper-parameter $k$) to make **reliable decisions**.
In that case, output the category that has the highest current representation instead.

#### Reduced Error Pruning
A post-pruning approach based on **cross-validation**:

1. Partition training data $D$ in learning $L$ and validation $V$ sets
2. Build a tree for $L$
3. Until **accuracy** of $V$ decreases, do:
   1. $\forall\ n \notin leaves$ 
      1. Temporarely prune $subtree(n)$ and replace with a leaf labeled as the current majority class
      2. Record **accuracy** of $subtree(n)$ on $V$  
   2. Permanently prune any $n$ that does not significantly improve accuracy over $V$ compared to the pruned tree

More accurate than pruning, but it consumes some training data for validation.

